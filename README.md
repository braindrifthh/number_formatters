# README #

This module provides an aditional scale option "Dynamic" to the core number decimal field formatter. This option shows only as many digits to the right of the decimal as there are. It removes all "0" on the right side of the decimal.